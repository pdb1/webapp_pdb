### 14.11.2023 - by Fuat Ustuner
import csv 
import numpy as np
from os import listdir
import os.path #import isfile, join, isdir
import pandas as pd
import altair as alt
import glob


# check data directory exists and files inside

dataPath=os.path.join(os.getcwd()+"/datafiles")
if os.path.isdir(dataPath):
    print("Found directory!")
    print([f for f in listdir(dataPath) if os.path.isfile(os.path.join(dataPath, f))])
else:
    print("Cannot find directory:",dataPath) 


fileNameReal="Shipment.csv"
df_csv_real=pd.read_csv(os.path.join(dataPath, fileNameReal))



# read the csv into pandas dataframe
fileName="Shipment.csv"
df_csv=pd.read_csv(os.path.join(dataPath, fileName)) #, names=list('abcdef'))
df_csv.columns = df_csv.columns.str.replace(' ', '_')
df_csv=df_csv[df_csv['Serial_number'].notna()].reset_index(drop=True)

print(df_csv.columns)


### get set non-serialNumber breaks in dataframe
df_nonSN=df_csv[~df_csv['Serial_number'].str.startswith(('20UP'))]

# clean-up dataframe and define serialNumbers
df_SNs=df_csv[df_csv['Serial_number'].str.startswith(('20UP'))]
df_SNs


'''
# clean-up dataframe and define serialNumbers
df_SNs=df_csv[df_csv['Serial_number'].str.startswith(('20-U-'))]
df_SNs['serialNumber']=df_SNs['Serial_number'].str.replace('-','')

df_SNs '''

# list for sorting by vendor
vendorList=[]
first=True
for x in df_nonSN['Serial_number'].unique():
    print(x)
    idx = df_csv.index[df_csv['Serial_number']==x][0]
   # print(idx)
    if not first:
        vendorList[-1]['end']=idx
        
    else:
        first=False
        
    vendorList.append({'name':x, 'start':idx+1})
    

[print(v) for v in vendorList]


# list for sorting by location
locList=[]
first=True
for x in df_SNs['Location'].unique():
    if isinstance(x, (str, bytes)) and x.strip() != '':
        print(x)
        sns = df_SNs.query('Location=="'+x+'"')['Serial_number'].to_list()
        locList.append({'name':x, 'SNs':sns})

[print(l) for l in locList]


# Group serialNumbers
writePath=os.path.abspath(os.getcwd()+"/outfiles/")
sep=","
regSize=50
headerList=["serialNumber"]
for l in locList:
    print("set:",l['name'])
    l['count']=0
    count=0
    
    for sn in l['SNs']:
        print("\t"+sn)
        if count%regSize==0:
            try:
                f.close()
            except NameError:
                pass
            except AttributeError:
                pass
            f = open(os.path.join(writePath, "Shipment_to_"+l['name']+"_"+str(int(count/regSize))+".csv"), "w")
            f.write(sep.join(headerList)+"\n")
        f.write(sep.join([sn])+"\n")
        l['count']+=1
        count+=1
        print(count)
    f.close()

