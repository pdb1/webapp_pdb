### 0. Imports
import csv 
import numpy as np
from os import listdir
import os.path #import isfile, join, isdir
import pandas as pd
import altair as alt
import glob

### 1. Get data file
# check data directory exists and files inside

dataPath=os.path.join(os.getcwd()+"/datafiles")
if os.path.isdir(dataPath):
    print("Found directory!")
    print([f for f in listdir(dataPath) if os.path.isfile(os.path.join(dataPath, f))])
else:
    print("Cannot find directory:",dataPath)

fileNameReal="ITkPixV1_Preproduction_QC - NCAB_OS_reg.csv"
df_csv_real=pd.read_csv(os.path.join(dataPath, fileNameReal))


### read the csv into pandas dataframe
fileName="ITkPixV1_Preproduction_QC - NCAB_OS_reg.csv"
df_csv=pd.read_csv(os.path.join(dataPath, fileName)) #, names=list('abcdef'))
df_csv.columns = df_csv.columns.str.replace(' ', '_')
df_csv=df_csv[df_csv['Serial_number'].notna()].reset_index(drop=True)
print(df_csv.columns)

### get set non-serialNumber breaks in dataframe
df_nonSN=df_csv[~df_csv['Serial_number'].str.startswith(('20UP'))]

# clean-up dataframe and define serialNumbers
df_SNs=df_csv[df_csv['Serial_number'].str.startswith(('20UP'))]
df_SNs

# list for sorting by vendor
vendorList=[]
first=True
for x in df_nonSN['Serial_number'].unique():
    print(x)
    idx = df_csv.index[df_csv['Serial_number']==x][0]
   # print(idx)
    if not first:
        vendorList[-1]['end']=idx
        
    else:
        first=False
        
    vendorList.append({'name':x, 'start':idx+1})
    

[print(v) for v in vendorList]

# list for sorting by location
locList=[]
first=True
for x in df_SNs['Location'].unique():
    print(x)
    sns = df_SNs.query('Location=="'+x+'"')['Serial_number'].to_list()
    locList.append({'name':x, 'SNs':sns})

[print(l) for l in locList]

Registration_results=[]
Registration_empty_array=[]
Registration_reshape_empty_array=[]

for i in range(0,len(df_SNs)): #For Copper thickness RD53A test results, the Copper thickness RD53A test results of each flex are recorded one by one.
    
    Registration_results=[df_SNs['Serial_number'][i],"PCB","PG", "QUAD_PCB","","PCB_DESIGN_VERSION",df_SNs['PCB_Design_Version_\n(For_ITkPixV1_-_put_1)'][i],"PCB_VENDOR_TECHNOLOGY",df_SNs['PCB_Vendor_Technology\nEPEC_(100_um_trace_width)_--_1\nNCAB_(100_um_trace_width_for_quads_or_triplets)_--_2\nATLAFLEX_(75_um_trace_width)_--_3\nSFCircuits_(100_um_trace_width)_--_4\nPHOENIX_(100_um_trace_width_for_quads_or_triplets)_--5\nYamashita_Material_(75_um_trace_width)_--_6'][i],"PCB_MANUFACTURER",df_SNs['PCB_Manufacturer_\n(Name_of_Vendor)'][i],"","","","","True"]
        
    Registration_empty_array = np.append(Registration_empty_array, Registration_results) 

Registration_reshape_empty_array=np.reshape(Registration_empty_array ,(len(df_SNs), 16))

print(Registration_reshape_empty_array)

writePath=os.path.abspath(os.getcwd()+"/outfiles") #creating new csv file about Copper Thickness RD53A Test

rows = Registration_reshape_empty_array
    
with open(os.path.join(writePath, "Registration_v3.2_Unpopulated"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)

import csv
divisor = 50
file_counter = 0
headerList=["serialNumber","componentType","subproject","type","alternativeIdentifier","property1_key","property1_value","property2_key","property2_value","property3_key","property3_value","property4_key","property4_value","property5_key","property5_value","asStrings"]

with open(os.path.join(writePath, "Registration_v3.2_Unpopulated"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Registration_v3.2_Unpopulated_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
    
# Delete main files
writePath=os.path.abspath(os.getcwd()+"/outfiles/")
os.remove(os.path.join(writePath, 'Registration_v3.2_Unpopulated.csv'))
