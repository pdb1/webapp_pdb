#Import Libs
import csv 
import numpy as np
from os import listdir
import os.path #import isfile, join, isdir
import pandas as pd
import altair as alt
import glob


# 1. Get data file
# check data directory exists and files inside

dataPath=os.path.join(os.getcwd()+"/datafiles")
if os.path.isdir(dataPath):
    print("Found directory!")
    print([f for f in listdir(dataPath) if os.path.isfile(os.path.join(dataPath, f))])
else:
    print("Cannot find directory:",dataPath)

# Read the csv into pandas dataframe
fileName="ITkPixV1_Preproduction_QC - NCAB_OS_PopBatch1.csv"
df_csv=pd.read_csv(os.path.join(dataPath, fileName)) #, names=list('abcdef'))
df_csv.columns = df_csv.columns.str.replace(' ', '_')
df_csv=df_csv[df_csv['Date_Processed'].notna()].reset_index(drop=True)
print(df_csv.columns)


# Remove - between numbers on the serial number
df_SNs=df_csv[df_csv['Serial_number'].str.startswith(('20UP'))]
df_SNs.replace(np.NaN, '', inplace=True)
df_SNs


# Control how many rows we have
print(len(df_SNs))



# Create HV-LV Test CSV File

HV_Test_results=[]
HV_Test_empty_array=[]
HV_Test_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''
HV_leakage=''
R1_HV_Resistor=0


for i in range(0,len(df_SNs)): #For HV_TEST results, the HV test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
        
    if df_SNs['Capacitor_leakage_test_(mV_on_readout_card)'][i]=='HV keep rising':
        HV_leakage=''
    else:
        HV_leakage=df_SNs['Capacitor_leakage_test_(mV_on_readout_card)'][i]
    #if df_SNs['Vin_Drop_(V)'][i]!= '' and df_SNs['GND_Drop_(V)'][i]!='':
    #    effective_resistance =((df_SNs['Vin_Drop_(V)'][i]/50)+(df_SNs['GND_Drop_(V)'][i]/50))*1000
    if df_SNs['NTC_reading_(V)'][i]!='':
        ntc_value=((0.2*51)/df_SNs['NTC_reading_(V)'][i])
    
    if df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][0:4]!='':
        R1_HV_Resistor=df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][0:2]
    elif df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][0:4]=='':
        R1_HV_Resistor=-1
    HV_Test_results=[i,df_SNs['Serial_number'][i],
                     'PCB',
                     'PCB_QC',
                     'HV_LV_TEST','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'VIN_DROP',df_SNs['Vin_Drop_(V)'][i],
                     'GND_DROP',df_SNs['GND_Drop_(V)'][i],
                     'EFFECTIVE_RESISTANCE',df_SNs['Effective_resistance_(mOhms)'][i],
                     'HV_LEAKAGE',HV_leakage,
                     'LEAKAGE_CURRENT',df_SNs['Leakage_current_on_Keithley,_after_the_HV_test_(nA)'][i],
                     'NTC_VOLTAGE',df_SNs['NTC_reading_(V)'][i],
                     'NTC_VALUE',round(ntc_value,3),
                     'TEMPERATURE',df_SNs['Temp_(°C)_(@HV_test)'][i],
                     'HUMIDITY',df_SNs['humidity_(%)_(@HV_test)'][i],
                     'R1_HV_RESISTOR',R1_HV_Resistor,
                     '','',
                     '',''] #HV test results for each flex
    
    HV_Test_empty_array = np.append(HV_Test_empty_array, HV_Test_results) 

HV_Test_reshape_empty_array=np.reshape(HV_Test_empty_array ,(len(df_SNs), 40))

print(HV_Test_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles/") #creating new csv file about HV_Test_Results

rows = HV_Test_reshape_empty_array
    
with open(os.path.join(writePath, "HV_LV_Test_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    

# Save this HV-LV Test CSV file inside the folder

divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "HV_LV_Test_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('HV_LV_Test_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()


'''
# Create Layer Thickness Test CSV File
Layer_results=[]
Layer_empty_array=[]
Layer_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''

#inputs of this test type
Top_Layer=0
Inner_Layer=0
Bottom_Layer=0
Dielectric_Thickness=0
Coverlay=0
Thickness=0
#inputs of this test type

for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    #if there is no info, directly -1 is defined
    if df_SNs['Top_Cu_thickness_(including_ENIG)_(um)'][i]*1!='':
        Top_Layer= df_SNs['Top_Cu_thickness_(including_ENIG)_(um)'][i]*1
    elif df_SNs['Top_Cu_thickness_(including_ENIG)_(um)'][i]*1=='':
        Top_Layer= -1
        
    if df_SNs['Inner_layer_thickness_(Mcu)_(um)'][i]*1!='':
        Inner_Layer= df_SNs['Inner_layer_thickness_(Mcu)_(um)'][i]*1
    elif df_SNs['Inner_layer_thickness_(Mcu)_(um)'][i]*1=='':
        Inner_Layer= -1
        
    if df_SNs['Bott_Cu_thickness_(including_ENIG)_(um)'][i]*1!='':
        Bottom_Layer= df_SNs['Bott_Cu_thickness_(including_ENIG)_(um)'][i]*1
    elif df_SNs['Bott_Cu_thickness_(including_ENIG)_(um)'][i]*1=='':
        Bottom_Layer= -1
    
    if df_SNs['Dielectric_thickness(um)'][i]*1!='':
        Dielectric_Thickness= df_SNs['Dielectric_thickness(um)'][i]*1
    elif df_SNs['Dielectric_thickness(um)'][i]*1=='':
        Dielectric_Thickness= -1
        
    if df_SNs['Coverlay_with_Adhesive_thickness(um)'][i]*1!='':
        Coverlay= df_SNs['Coverlay_with_Adhesive_thickness(um)'][i]*1
    elif df_SNs['Coverlay_with_Adhesive_thickness(um)'][i]*1=='':
        Coverlay= -1
        
    if df_SNs['Total_Thickness(um)'][i]*1!='':
        Thickness= df_SNs['Total_Thickness(um)'][i]*1
    elif df_SNs['Total_Thickness(um)'][i]*1=='':
        Thickness= -1
    
    Layer_results=[i,df_SNs['Serial_number'][i],
                     'PCB',
                     'PCB_RECEPTION',
                     'LAYER_THICKNESS','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'TOP_LAYER_THICKNESS',Top_Layer,
                     'INNER_LAYER_THICKNESS',Inner_Layer,
                     'BOTTOM_LAYER_THICKNESS',Bottom_Layer,
                     'DIELECTRIC_THICKNESS',Dielectric_Thickness,
                     'COVERLAY_WITH_ADHESIVE_THICKNESS',Coverlay,
                     'SOLDERMASK_THICKNESS',-1,
                     'THICKNESS',Thickness,
                     '','',
                     '','',
                     '','',
                     '','',
                     '',''] #Test results for each flex
    
    Layer_empty_array = np.append(Layer_empty_array, Layer_results) 

Layer_reshape_empty_array=np.reshape(Layer_empty_array ,(len(df_SNs), 40))

print(Layer_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles/") #creating new csv file 

rows = Layer_reshape_empty_array
    
with open(os.path.join(writePath, "Layer_Thickness_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    

# Save this Layer Thickness Test CSV file inside the folder
import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Layer_Thickness_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Layer_Thickness_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close() 
        

# Create Visual Inspection Test CSV File
Visual_Inspection_results=[]
Visual_Inspection_empty_array=[]
Visual_Inspection_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.

    if df_SNs['Overall_grade'][i]<3:
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Overall_grade'][i]==3:
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    Visual_Inspection_results=[i,df_SNs['Serial_number'][i],
                     'PCB',
                     'PCB_RECEPTION',
                     'VISUAL_INSPECTION','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'WIREBOND_PADS_CONTAMINATION_GRADE',df_SNs['Wirebond_pads_clear_\nof_contamination_grade'][i],
                     'PARTICULATE_CONTAMINATION_GRADE',df_SNs['Particulate_\ncontamination_\ngrade'][i],
                     'WATERMARKS_GRADE',df_SNs['Watermarks_\ngrade'][i],
                     'SCRATCHES_GRADE',df_SNs['Scratches_\ngrade'][i],
                     'SOLDERMASK_IRREGULARITIES_GRADE',df_SNs['Soldermask_\nirregularity/cracks_\ngrade'][i],
                     'HV_LV_CONNECTOR_ASSEMBLY_GRADE',df_SNs['HV_LV_connector_\nassembly_issue_\ngrade'][i],
                     'DATA_CONNECTOR_ASSEMBLY_GRADE',df_SNs['Data_connector_\nassembly_issue_\ngrade'][i],
                     'SOLDER_SPILLS_GRADE',df_SNs['Solder_\nspills_\ngrade'][i],
                     'COMPONENT_MISALIGNMENT_GRADE',df_SNs['Component_\nmisalignment_\ngrade'][i],
                     'SHORTS_OR_CLOSE_PROXIMITY_GRADE',df_SNs['Shorts/close_\nproximity_of_\ncomponents_\ndue_to_\nmisalignment_\ngrade'][i],
                     'OVERALL_GRADE',df_SNs['Overall_grade'][i],
                     'OBSERVATION',df_SNs['Comment'][i]] #Test results for each flex
    
    Visual_Inspection_empty_array = np.append(Visual_Inspection_empty_array, Visual_Inspection_results) 

Visual_Inspection_reshape_empty_array=np.reshape(Visual_Inspection_empty_array ,(len(df_SNs), 40))

print(Visual_Inspection_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles/") #creating new csv file 

rows = Visual_Inspection_reshape_empty_array
    
with open(os.path.join(writePath, "Visual_Inspection_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    

# Save this Visual Inspection Test CSV file inside the folder
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Visual_Inspection_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Visual_Inspection_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
'''        


# Delete main files
writePath=os.path.abspath(os.getcwd()+"/outfiles/")
os.remove(os.path.join(writePath, 'HV_LV_Test_Results.csv'))
#os.remove(os.path.join(writePath, 'Layer_Thickness_Results.csv'))
#os.remove(os.path.join(writePath, 'Visual_Inspection_Results.csv'))
