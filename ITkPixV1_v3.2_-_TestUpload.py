#!/usr/bin/env python
# coding: utf-8

# In[1]:


### 0. Imports
import csv 
import numpy as np
from os import listdir
import os.path #import isfile, join, isdir
import pandas as pd
import altair as alt
import glob


# In[2]:


### 1. Get data file
# check data directory exists and files inside

dataPath=os.path.join(os.getcwd()+"/datafiles")
if os.path.isdir(dataPath):
    print("Found directory!")
    print([f for f in listdir(dataPath) if os.path.isfile(os.path.join(dataPath, f))])
else:
    print("Cannot find directory:",dataPath) 


# In[3]:


fileNameReal="ITkPixV1_Rev3.2 Quad Hybrid QC register - Sayfa1_28.05.csv"
df_csv_real=pd.read_csv(os.path.join(dataPath, fileNameReal))
display(df_csv_real)


# In[4]:


### read the csv into pandas dataframe
fileName="ITkPixV1_Rev3.2 Quad Hybrid QC register - Sayfa1_28.05.csv"
df_csv=pd.read_csv(os.path.join(dataPath, fileName)) #, names=list('abcdef'))
df_csv.columns = df_csv.columns.str.replace(' ', '_')
df_csv=df_csv[df_csv['Date_Processed'].notna()].reset_index(drop=True)
display(df_csv)
print(df_csv.columns)


# In[5]:


df_SNs=df_csv[df_csv['Serial_number'].str.startswith(('20-U'))]
df_SNs['serialNumber']=df_SNs['Serial_number'].str.replace('-','')
df_SNs.replace(np.NaN, '', inplace=True)
df_SNs


# In[6]:


print(len(df_SNs))


# # HV_LV_Test CSV 

# In[7]:


HV_Test_results=[]
HV_Test_empty_array=[]
HV_Test_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''
HV_leakage=''


for i in range(0,len(df_SNs)): #For HV_TEST results, the HV test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
        
    if df_SNs['Capacitor_leakage_test_(mV_on_readout_card)'][i]=='HV keep rising':
        HV_leakage=''
    else:
        HV_leakage=df_SNs['Capacitor_leakage_test_(mV_on_readout_card)'][i]
    
    effective_resistance =((df_SNs['Vin_Drop_(V)'][i]/50)+(df_SNs['GND_Drop_(V)'][i]/50))*1000
    

    HV_Test_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'PCB_QC',
                     'HV_LV_TEST','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'VIN_DROP',df_SNs['Vin_Drop_(V)'][i],
                     'GND_DROP',df_SNs['GND_Drop_(V)'][i],
                     'EFFECTIVE_RESISTANCE',round(effective_resistance,3),
                     'HV_LEAKAGE',HV_leakage,
                     'LEAKAGE_CURRENT',df_SNs['Leakage_current_on_Keithley,_after_the_HV_test_(nA)'][i],
                     'NTC_VOLTAGE',df_SNs['NTC_reading_(V)'][i],
                     'NTC_VALUE',df_SNs['NTC_resistor_value_(TH1)_(Kohms)'][i],
                     'TEMPERATURE',df_SNs['Temp_(°C)_(@HV_test)'][i],
                     'HUMIDITY',df_SNs['humidity_(%)_(@HV_test)'][i],
                     'R1_HV_RESISTOR',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][0:4],
                     '','',
                     '',''] #HV test results for each flex
    
    HV_Test_empty_array = np.append(HV_Test_empty_array, HV_Test_results) 

HV_Test_reshape_empty_array=np.reshape(HV_Test_empty_array ,(len(df_SNs), 40))

print(HV_Test_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file about HV_Test_Results

rows = HV_Test_reshape_empty_array
    
with open(os.path.join(writePath, "HV_LV_Test_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[8]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "HV_LV_Test_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('HV_LV_Test_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # Mass CSV

# In[9]:


Mass_results=[]
Mass_empty_array=[]
Mass_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For TEST results, the HV test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    if type(df_SNs['Mass_(g)'][i])==str:
        mass=0
    elif type(df_SNs['Mass_(g)'][i])!=str:
        mass=df_SNs['Mass_(g)'][i]/1000
        
    Mass_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'PCB_RECEPTION_MODULE_SITE',
                     'MASS','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'MASS',mass,
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '',''] #Test results for each flex
    
    Mass_empty_array = np.append(Mass_empty_array, Mass_results) 

Mass_reshape_empty_array=np.reshape(Mass_empty_array ,(len(df_SNs), 40))

print(Mass_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file about Test_Results

rows = Mass_reshape_empty_array
    
with open(os.path.join(writePath, "Mass_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[10]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Mass_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Mass_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # NTC Verification CSV

# In[11]:


NTC_Verification_results=[]
NTC_Verification_empty_array=[]
NTC_Verification_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For TEST results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    NTC_Verification_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'QA_PRE_THERMAL_CYCLE',
                     'NTC_VERIFICATION','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'NTC_VALUE',df_SNs['NTC_resistor_value_(TH1)_(Kohms)'][i],
                     'NTC_TEMP',[df_SNs['Temp_(°C)_(@HV_test)'][i],df_SNs['Temp_(°C)_(@HV_test)'][i]],
                     'HUMIDITY',df_SNs['humidity_(%)_(@HV_test)'][i],
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '',''] #Test results for each flex
    
    NTC_Verification_empty_array = np.append(NTC_Verification_empty_array, NTC_Verification_results) 

NTC_Verification_reshape_empty_array=np.reshape(NTC_Verification_empty_array ,(len(df_SNs), 40))

print(NTC_Verification_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file about Test_Results

rows = NTC_Verification_reshape_empty_array
    
with open(os.path.join(writePath, "NTC_Verification_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[12]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "NTC_Verification_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('NTC_Verification_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # SLDO Resistor CSV
# 

# In[13]:


SLDO_results=[]
SLDO_empty_array=[]
SLDO_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    SLDO_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'QA_PRE_THERMAL_CYCLE',
                     'SLDO_RESISTORS','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'R47_ANALOG',df_SNs['SLDO_resistor_value_R47,_R38,_R29,_R20_(Ohms)'][i],
                     'R51_DIGITAL',df_SNs['SLDO_resistor_vaule_R51,_R42,_R33,_R24_(Ohms)'][i],
                     'R44',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][24:28],
                     'R45',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][19:21],
                     'R46',df_SNs['Precision_Resistor_value_R46_&_R5_/_R50_&_R48_(kOhms)'][i][0:4],
                     'R48',df_SNs['Precision_Resistor_value_R46_&_R5_/_R50_&_R48_(kOhms)'][i][7:9],
                     'R49',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][31:34],
                     'R7',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][7:9],
                     'R52',df_SNs['Precision_Resistor_value_R52_(Ohm)'][i],
                     'TH5_NTC',df_SNs['Precision_Resistors_value_R1/_R7/TH5/R45/R44/R49_(kOhms)'][i][12:16],
                     '','',
                     '',''] #Test results for each flex
    
    SLDO_empty_array = np.append(SLDO_empty_array, SLDO_results) 

SLDO_reshape_empty_array=np.reshape(SLDO_empty_array ,(len(df_SNs), 40))

print(SLDO_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file 

rows = SLDO_reshape_empty_array
    
with open(os.path.join(writePath, "SLDO_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[14]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "SLDO_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('SLDO_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # Visual Inspection CSV

# In[15]:


Visual_Inspection_results=[]
Visual_Inspection_empty_array=[]
Visual_Inspection_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    Visual_Inspection_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'PCB_RECEPTION_MODULE_SITE',
                     'VISUAL_INSPECTION','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'WIREBOND_PADS_CONTAMINATION_GRADE','1',
                     'PARTICULATE_CONTAMINATION_GRADE','1',
                     'WATERMARKS_GRADE','2',
                     'SCRATCHES_GRADE','2',
                     'SOLDERMASK_IRREGULARITIES_GRADE','1',
                     'HV_LV_CONNECTOR_ASSEMBLY_GRADE','2',
                     'DATA_CONNECTOR_ASSEMBLY_GRADE','3',
                     'SOLDER_SPILLS_GRADE','3',
                     'COMPONENT_MISALIGNMENT_GRADE','2',
                     'SHORTS_OR_CLOSE_PROXIMITY_GRADE','2',
                     'OVERALL_GRADE','3',
                     'OBSERVATION','OK'] #Test results for each flex
    
    Visual_Inspection_empty_array = np.append(Visual_Inspection_empty_array, Visual_Inspection_results) 

Visual_Inspection_reshape_empty_array=np.reshape(Visual_Inspection_empty_array ,(len(df_SNs), 40))

print(Visual_Inspection_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file 

rows = Visual_Inspection_reshape_empty_array
    
with open(os.path.join(writePath, "Visual_Inspection_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[16]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Visual_Inspection_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Visual_Inspection_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # Qucik Visual Inspection

# In[17]:


Quick_Visual_Inspection_results=[]
Quick_Visual_Inspection_empty_array=[]
Quick_Visual_Inspection_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    Quick_Visual_Inspection_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'PCB_CUTTING',
                     'QUICK_INSPECTION','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'OBSERVATION','OK',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '','',
                     '',''] #Test results for each flex
    
    Quick_Visual_Inspection_empty_array = np.append(Quick_Visual_Inspection_empty_array, Quick_Visual_Inspection_results) 

Quick_Visual_Inspection_reshape_empty_array=np.reshape(Quick_Visual_Inspection_empty_array ,(len(df_SNs), 40))

print(Quick_Visual_Inspection_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file 

rows = Quick_Visual_Inspection_reshape_empty_array
    
with open(os.path.join(writePath, "Quick_Visual_Inspection_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[18]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Quick_Visual_Inspection_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Quick_Visual_Inspection_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# # Layer Thickness CSV

# In[19]:


Layer_results=[]
Layer_empty_array=[]
Layer_reshape_empty_array=[]
Pass_Status=''
Problem_Status=''


for i in range(0,len(df_SNs)): #For Test results, the test results of each flex are recorded one by one.
    if df_SNs['Status'][i]=='Pass' or df_SNs['Status'][i]=='pass':
        Pass_Status='TRUE'
        Problem_Status='FALSE'
    elif df_SNs['Status'][i]=='Fail':
        Pass_Status='FALSE'
        Problem_Status='TRUE'
    
        
    Layer_results=[i,df_SNs['serialNumber'][i],
                     'PCB',
                     'QA_PRE_THERMAL_CYCLE',
                     'LAYER_THICKNESS','EDI',
                     df_SNs['Run_Number(How_many_times_does_flex_tested)_If__two_time_we_need_to_update_test_date_in_terms_of_the_date_when_second_test_is_done.(1.0_/_2.0_/_3.0_for_ones,_twice_or_three_times_respectively)'][i],
                     df_SNs['Date_Processed'][i],
                     Pass_Status,
                     Problem_Status,
                     df_SNs['Operator_Name'][i],'OPERATOR',
                     'INSTRUMENT','',
                     'ANALYSIS_VERSION','',
                     'TOP_LAYER_THICKNESS',df_SNs['Top_Cu_thickness_(including_ENIG)_(mm)'][i]*1000,
                     'INNER_LAYER_THICKNESS',df_SNs['Inner_layer_thickness_(Mcu)'][i]*1000,
                     'BOTTOM_LAYER_THICKNESS',df_SNs['Bott_Cu_thickness_(including_ENIG)_(mm)'][i]*1000,
                     'DIELECTRIC_THICKNESS',df_SNs['Dielectric_thickness(mm)'][i]*1000,
                     'COVERLAY_WITH_ADHESIVE_THICKNESS',df_SNs['Coverlay_with_Adhesive_thickness(mm)'][i]*1000,
                     'SOLDERMASK_THICKNESS',0,
                     'THICKNESS',df_SNs['Total_Thickness(mm)'][i]*1000,
                     '','',
                     '','',
                     '','',
                     '','',
                     '',''] #Test results for each flex
    
    Layer_empty_array = np.append(Layer_empty_array, Layer_results) 

Layer_reshape_empty_array=np.reshape(Layer_empty_array ,(len(df_SNs), 40))

print(Layer_reshape_empty_array)


writePath=os.path.abspath(os.getcwd()+"/outfiles_ITkPixV1_v3.2/") #creating new csv file 

rows = Layer_reshape_empty_array
    
with open(os.path.join(writePath, "Layer_Thickness_Results"+".csv"), "w") as f: 
    write = csv.writer(f) 
    write.writerows(rows)  
print(writePath)    


# In[20]:


import csv
divisor = 50
file_counter = 0
headerList=["","component","componentType","stage","testType","institution","runNumber","date","passed","problems",
            "property1_value","property1_key","property2_value","property2_key","property3_value","property3_key",
            "result1_key","result1_value","result2_key","result2_value","result3_key","result3_value","result4_key",
            "result4_value","result5_key","result5_value","result6_key","result6_value","result7_key","result7_value",
            "result8_key","result8_value","result9_key","result9_value","result10_key","result10_value","result11_key","result11_value","result12_key","result12_value"] 

with open(os.path.join(writePath, "Layer_Thickness_Results"+".csv"), 'r') as infile:
    for index, rows in enumerate(csv.reader(infile)):   
        if index % divisor == 0: 
            outfilename = ('Layer_Thickness_Results_{}.csv'.format(file_counter))
            f = open(os.path.join(writePath, outfilename), 'w')
            writer = csv.writer(f)
            writer.writerow(headerList)
            file_counter += 1
            
        writer.writerow(rows)
    f.close()
        


# In[ ]:





# In[ ]:




